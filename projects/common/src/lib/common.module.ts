import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { Interceptor } from './interceptors/interceptor';
import { DecimalOnlyDirective } from './directives/allow-decimal.directive';

@NgModule({
  declarations: [DecimalOnlyDirective],
  imports: [HttpClientModule],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: Interceptor, multi: true }
  ]
})
export class CommonModule {}
