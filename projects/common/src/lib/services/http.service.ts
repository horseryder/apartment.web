import { Injectable } from '@angular/core';
import { HttpClient, HttpBackend } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

// const apiUrl = 'http://localhost:60995/';
const apiUrl = 'http://qa.app.callerlocations.com/';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  constructor(private http: HttpClient, private handler: HttpBackend) {}

  get<T>(baseUrl: string, params?: any): Observable<T> {
    return this.http
      .get<T>(`${apiUrl}${baseUrl}`, { params })
      .pipe(
        map(data => {
          return data;
        })
      );
  }

  post<T>(baseUrl: string, body: any): Observable<T> {
    return this.http.post<T>(`${apiUrl}${baseUrl}`, body).pipe(
      map(data => {
        return data;
      })
    );
  }

  postLogin<T>(baseUrl: string, body: any, options?: any): Observable<T> {
    const httpClient = new HttpClient(this.handler);
    return httpClient
      .post<T>(`${apiUrl}${baseUrl}`, body, {
        headers: options.headers,
        params: options.params
      })
      .pipe(
        map(data => {
          return data;
        })
      );
  }
}
