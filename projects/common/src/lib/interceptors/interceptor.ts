import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpEvent
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class Interceptor implements HttpInterceptor {
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const userDetails = JSON.parse(localStorage.getItem('userDetails'));
    const customReq = request.clone({
        headers: request.headers
          .append(
            'Authorization',
            'Bearer ' + userDetails.AccessToken
          )
    });
    return next.handle(customReq);
  }
}
