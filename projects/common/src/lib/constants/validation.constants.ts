export enum Regex {
    DATEVALIDATION = '^(0[1-9]|1[0-2])(/|-)(0[1-9]|1\\d|2\\d|3[01])(/|-)(19|20)\\d{2}$',
    NUMBERVALIDATION = '^[0-9]*$',
    DECIMALVALIDATION = '^[0-9]+(\.[0-9]*){0,1}$',
    PHONEVALIDATION = '^([0|\+[0-9]{1,5})?([6-9][0-9]{9})$'
  }