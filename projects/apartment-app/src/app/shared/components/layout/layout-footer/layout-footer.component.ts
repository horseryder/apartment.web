import { Component, OnInit } from '@angular/core';

import { SidenavchangeService } from '../services/sidenavchange.service';

@Component({
  selector: 'app-layout-footer',
  templateUrl: './layout-footer.component.html',
  styleUrls: ['./layout-footer.component.scss']
})
export class LayoutFooterComponent implements OnInit {
  constructor(public sidenavchangeService: SidenavchangeService) {}

  ngOnInit() {}
}
