import { Component, OnInit } from '@angular/core';

import { SidenavchangeService } from '../services/sidenavchange.service';

@Component({
  selector: 'app-layout-content',
  templateUrl: './layout-content.component.html',
  styleUrls: ['./layout-content.component.scss']
})
export class LayoutContentComponent implements OnInit {
  constructor(public sidenavchangeService: SidenavchangeService) {}

  ngOnInit() {}
}
