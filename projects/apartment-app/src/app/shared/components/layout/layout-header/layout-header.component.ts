import { Component, OnInit } from '@angular/core';
import { of as observableOf } from 'rxjs';

import { SidenavchangeService } from '../services/sidenavchange.service';

@Component({
  selector: 'app-layout-header',
  templateUrl: './layout-header.component.html',
  styleUrls: ['./layout-header.component.scss']
})
export class LayoutHeaderComponent implements OnInit {
  toggle = false;
  constructor(private sidenavchangeService: SidenavchangeService) {}

  ngOnInit() {}

  toggleSideNav() {
    this.toggle = !this.toggle;
    this.sidenavchangeService.sideNavChange$ = observableOf(this.toggle);
    if (document.body.classList.contains('sidebar-collapse')) {
      document.body.classList.remove('sidebar-collapse');
    } else {
      document.body.classList.add('sidebar-collapse');
    }
  }
}
