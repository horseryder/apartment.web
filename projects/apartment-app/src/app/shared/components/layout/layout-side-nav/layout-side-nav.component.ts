import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

import { SidenavchangeService } from '../services/sidenavchange.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-layout-side-nav',
  templateUrl: './layout-side-nav.component.html',
  styleUrls: ['./layout-side-nav.component.scss']
})
export class LayoutSideNavComponent implements OnInit {
  @ViewChild('sideBar', { static: false }) sideBar: ElementRef;
  constructor(
    public sidenavchangeService: SidenavchangeService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  public userDetails: any;

  ngOnInit() {
    this.userDetails = JSON.parse(localStorage.getItem('userDetails'));
  }

  toggleMenu(element) {
    if (element.parentElement.classList.contains('active')) {
      element.parentElement.classList.remove('active');
      element.nextSibling.classList.remove('menu-open');
    } else {
      element.parentElement.classList.add('active');
      element.nextSibling.classList.add('menu-open');
    }
  }

  redirectTo(url: string) {
    this.router.navigate([`../${url}`], {
      relativeTo: this.activatedRoute
    });
  }
}
