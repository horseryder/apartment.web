import { Injectable } from '@angular/core';

import { HttpService } from '@apartment/common';
import { HttpHeaders, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  options: {
    headers?: HttpHeaders;
    params?: HttpParams;
  };

  public userDetails: any = [];
  constructor(private http: HttpService) {}

  login() {
    this.options = {
      headers: new HttpHeaders().set(
        'Content-Type',
        'application/x-www-form-urlencoded'
      ),
      params: new HttpParams()
        .set('PhoneType', '1')
        .set('Username', '9533523515')
        .set('grant_type', 'password')
        .set(
          'DeviceToken',
          'e_7GkZY-aPo%3AAPA91bFoGQ8H0IejOlp0-knuJOr6NFxRWQGhZP17_wQa2Bq1dEjcaZ9eT-V7U90zaPwGY5VjoPCfifWX-e70E3kCNjZzokoVvo5LQoI_CQk1ZAovzSTYHvaSla_PqIB6dw2XmzqLFZzb'
        )
        .set('Password', 'JS%401fe0b8869-1e80-4f72-b914-d53363384463')
        .set('DeviceId', 'fe0b8869-1e80-4f72-b914-d53363384463')
    };

    return this.http.postLogin('account/Authenticate', null, this.options);
  }

  setUserDetails(userDetails: any[]) {
    this.userDetails = userDetails;
    localStorage.setItem('userDetails', JSON.stringify(userDetails));
  }
}
