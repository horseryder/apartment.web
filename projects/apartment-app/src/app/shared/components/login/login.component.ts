import { Component, OnInit } from '@angular/core';

import { LoginService } from './login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  constructor(private service: LoginService, private router: Router) {}

  ngOnInit() {}

  login() {
    this.service.login().subscribe((response: any[]) => {
      console.log('login', response);
      this.service.setUserDetails(response);
      this.router.navigate(['/dashboard']);
    });
  }
}
