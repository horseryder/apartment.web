import { Injectable } from '@angular/core';

import { HttpService } from '@apartment/common';
import { EndPoint } from '../constants/web-api.constants';
import { ClaimApartment } from '../models/claim-apartment';

@Injectable({
  providedIn: 'root'
})
export class SearchApartmentService {
  constructor(private http: HttpService) {}

  public getApartment(name: string) {
    return this.http.get(EndPoint.searchApartment(name));
  }

  public claimApartment(claimDetails: ClaimApartment) {
    return this.http.post(EndPoint.claimApartment, claimDetails);
  }
}
