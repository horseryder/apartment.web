import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { process, State } from '@progress/kendo-data-query';
import {
  GridDataResult,
  DataStateChangeEvent
} from '@progress/kendo-angular-grid';
import { ToastrService } from 'ngx-toastr';

import { SearchApartmentService } from './search-apartment.service';
import { Observable, of } from 'rxjs';
import { ClaimApartment } from '../models/claim-apartment';


@Component({
  selector: 'app-search-apartment',
  templateUrl: './search-apartment.component.html',
  styleUrls: ['./search-apartment.component.scss']
})
export class SearchApartmentComponent implements OnInit {
  public gridData = [];
  public view: Observable<GridDataResult>;
  public state: State = {
    skip: 0,
    take: 10
  };
  searchForm: FormGroup;

  constructor(
    private service: SearchApartmentService,
    private formBuilder: FormBuilder,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
    this.searchForm = this.formBuilder.group({
      search: ''
    });
    this.searchForm.controls['search'].valueChanges.subscribe(name => {
      this.service.getApartment(name).subscribe((data: any) => {
        this.gridData = data.Apartments;
        this.loadData();
        console.log(data);
      });
    });
  }

  loadData() {
    this.view = of(process(this.gridData, this.state));
  }

  public dataStateChange(state: DataStateChangeEvent): void {
    this.state = state;
    this.loadData();
  }

  claimApartment(data: any) {
    const userDetails = JSON.parse(localStorage.getItem('userDetails'));
    const claimApartment: ClaimApartment = {
      apartmentId: data.UniqueId,
      members: [
        {
          name: userDetails.Username,
          email: '',
          phone: userDetails.Username
        }
      ]
    };
    this.service.claimApartment(claimApartment).subscribe(response => {
      this.toastr.success('Apartment Claimed!!');
      console.log('apartment claimed', response);
    });
  }
}
