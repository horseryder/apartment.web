import { Members } from './members';

export interface ClaimApartment
{
    apartmentId?: number;
    members?: Members[];
}
