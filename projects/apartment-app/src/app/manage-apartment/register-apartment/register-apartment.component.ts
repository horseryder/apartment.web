import { Component, OnInit } from '@angular/core';
import { Form, FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-register-apartment',
  templateUrl: './register-apartment.component.html',
  styleUrls: ['./register-apartment.component.scss']
})
export class RegisterApartmentComponent implements OnInit {
  registerApartmentForm: FormGroup;
  public showDetails = true;
  public showFlatType = false;
  public showExpType = false;
  public showMembers = false;
  public showAmount = false;
  public members: FormArray;
  public flatTypes = [
    { Id: 1, Value: '1 BHK' },
    { Id: 2, Value: '2 BHK' },
    { Id: 3, Value: '3 BHK' }
  ];
  public expenseTypes = [
    { Id: 1, Value: 'Fixed' },
    { Id: 2, Value: 'Variable' },
    { Id: 3, Value: 'Fixed with Variable' }
  ];
  constructor(private fb: FormBuilder, private toastr: ToastrService) {}

  ngOnInit() {
    this.createFormGroup();
  }

  get name() {
    return this.registerApartmentForm.get('name');
  }
  get details() {
    return this.registerApartmentForm.get('details');
  }
  get address() {
    return this.registerApartmentForm.get('address');
  }
  get expenseTypeId() {
    return this.registerApartmentForm.get('expenseTypeId');
  }
  get flatTypeId() {
    return this.registerApartmentForm.get('flatTypeId');
  }
  get amount() {
    return this.registerApartmentForm.get('amount');
  }

  createFormGroup() {
    this.registerApartmentForm = this.fb.group({
      name: ['', Validators.required],
      details: ['', Validators.required],
      address: ['', Validators.required],
      flatTypeId: [''],
      expenseTypeId: [''],
      amount: [],
      members: this.fb.array([
        this.createMembers()
      ]),
    });
  }

  createMembers(): FormGroup {
    return this.fb.group({
      name: [''],
      email: [''],
      phone: ['']
    });
  }

  addMember(): void {
    this.members = this.registerApartmentForm.get('members') as FormArray;
    this.members.push(this.createMembers());
  }

  removeMember(memberIndex) {
    this.members = this.registerApartmentForm.get('members') as FormArray;
    this.members.removeAt(memberIndex);
  }
  moveNext(tabType?: string) {
    if (this.registerApartmentForm.valid) {
      if (tabType === 'flattype') {
        this.showDetails = !this.showDetails;
        this.showFlatType = !this.showFlatType;
        this.registerApartmentForm.get('flatTypeId').setValidators(Validators.required);
        this.registerApartmentForm.get('flatTypeId').updateValueAndValidity()
      } else if (tabType === 'expensetype') {
        this.showFlatType = !this.showFlatType;
        this.showExpType = !this.showExpType;
        this.registerApartmentForm.get('expenseTypeId').setValidators(Validators.required);
        this.registerApartmentForm.get('expenseTypeId').updateValueAndValidity();
      } else if (tabType === 'members') {
        this.showExpType = !this.showExpType;
        this.showMembers = !this.showMembers;
      }
    } else {
      this.toastr.warning('Input required Fields');
    }
  }

  selectedOption(expId) {

    const expenseType = this.expenseTypes.find(item => item.Id === Number(expId)).Value;
    if (expenseType === 'Fixed' || expenseType === 'Fixed with Variable') {
      this.showAmount = true;
      this.registerApartmentForm.get('amount').setValidators(Validators.required);
      this.registerApartmentForm.get('amount').updateValueAndValidity();
    } else {
      this.showAmount = false;
      this.registerApartmentForm.get('amount').setValidators(null);
      this.registerApartmentForm.get('amount').setValue(null);
      this.registerApartmentForm.get('amount').updateValueAndValidity();
    }
  }
}
