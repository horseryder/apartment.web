import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterApartmentComponent } from './register-apartment.component';

describe('RegisterApartmentComponent', () => {
  let component: RegisterApartmentComponent;
  let fixture: ComponentFixture<RegisterApartmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterApartmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterApartmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
