import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '@apartment/common';
import { ManageApartmentRoutingModule } from './manage-apartment-routing.module';
import { SearchApartmentComponent } from './search-apartment/search-apartment.component';
import { RegisterApartmentComponent } from './register-apartment/register-apartment.component';

@NgModule({
  declarations: [SearchApartmentComponent, RegisterApartmentComponent],
  imports: [CommonModule, SharedModule, ManageApartmentRoutingModule]
})
export class ManageApartmentModule {}
