export const apartment = 'apartment/';
export const apartmentInvitation = 'apartmentinvitation/';
export const save = 'save';

export const EndPoint = {
  searchApartment: ( name: string ) => `${apartment}search?name=${name}`,
  claimApartment: `${apartmentInvitation}${save}`
};
