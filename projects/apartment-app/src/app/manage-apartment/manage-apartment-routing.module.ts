import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SearchApartmentComponent } from './search-apartment/search-apartment.component';
import { RegisterApartmentComponent } from './register-apartment/register-apartment.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: SearchApartmentComponent
      },
      {
        path: 'register-apartment',
        component: RegisterApartmentComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManageApartmentRoutingModule {}
