import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LayoutComponent } from './shared/components/layout/layout/layout.component';
import { LayoutContentComponent } from './shared/components/layout/layout-content/layout-content.component';
import { LayoutControlSidebarComponent } from './shared/components/layout/layout-control-sidebar/layout-control-sidebar.component';
import { LayoutFooterComponent } from './shared/components/layout/layout-footer/layout-footer.component';
import { LayoutHeaderComponent } from './shared/components/layout/layout-header/layout-header.component';
import { LayoutSideNavComponent } from './shared/components/layout/layout-side-nav/layout-side-nav.component';
import { DashboardComponent } from './shared/components/dashboard/dashboard.component';

import { CommonModule, SharedModule } from '@apartment/common';
import { LoginComponent } from './shared/components/login/login.component';
import { LogoutComponent } from './shared/components/logout/logout.component';
import { ManageApartmentModule } from './manage-apartment/manage-apartment.module';
import { ToastrModule } from 'ngx-toastr';

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    LayoutContentComponent,
    LayoutControlSidebarComponent,
    LayoutFooterComponent,
    LayoutHeaderComponent,
    LayoutSideNavComponent,
    DashboardComponent,
    LoginComponent,
    LogoutComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    CommonModule,
    SharedModule,
    ManageApartmentModule,
    ToastrModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
