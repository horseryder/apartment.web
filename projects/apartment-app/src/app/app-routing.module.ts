import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LayoutComponent } from '../../src/app/shared/components/layout/layout/layout.component';
import { DashboardComponent } from './shared/components/dashboard/dashboard.component';
import { LoginComponent } from './shared/components/login/login.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: 'apartment',
        loadChildren:
          './manage-apartment/manage-apartment.module#ManageApartmentModule',
        data: { preload: true, delay: true }
      },
      { path: 'dashboard', component: DashboardComponent }
    ]
  },
  {
    path: 'dashboard',
    redirectTo: 'dashboard'
  },
  // {
  //   path: '**',
  //   redirectTo: 'dashboard'
  // }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
